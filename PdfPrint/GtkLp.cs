﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using System.Diagnostics;
using System.Collections.Generic;


namespace Hiperscan.PdfPrint
{    
    
    public class GtkLp
    {
        public static void PrintTo(string file_name)
        {
            List<string> file_names = new List<string>();
            file_names.Add(file_name);
            GtkLp.PrintTo(file_names);
        }
            
        public static void PrintTo(List<string> file_names)
        {
            string arguments = null;

            Process gtklp = new Process();
            gtklp.StartInfo.FileName  = "gtklp";
            gtklp.StartInfo.Arguments = "-l";
            gtklp.StartInfo.CreateNoWindow  = true;
            gtklp.StartInfo.UseShellExecute = false;
            gtklp.StartInfo.RedirectStandardOutput = true;
            gtklp.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;

            gtklp.Start();

            arguments = gtklp.StandardOutput.ReadLine();

            gtklp.WaitForExit();

            if (string.IsNullOrEmpty(arguments))
                return;

            Console.WriteLine("lpr " + arguments);

            Process lpr = new Process();
            lpr.StartInfo.FileName  = "lpr";
            string file_args = string.Empty;
            foreach (string file_name in file_names)
                file_args += file_args + " \"" + file_name + "\"";
            lpr.StartInfo.Arguments = arguments + file_args;
            lpr.StartInfo.CreateNoWindow  = true;
            lpr.StartInfo.UseShellExecute = false;
            lpr.Start();
        }
    }
}