﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using Hiperscan.Unix;
using System.Reflection;
using System.IO;

namespace Hiperscan.PdfPrint
{
    public abstract class PdfPrinting
    {
        protected PdfPrinting(string document)
        {
            if (document.StartsWith("\"", StringComparison.InvariantCulture) == false)
                document = "\"" + document;

            if (document.EndsWith("\"", StringComparison.InvariantCulture) == false)
                document += "\"";

            this.Document = document;
        }

        private void Print()
        {
            this.PrinterProcess().Start();
        }

        protected abstract System.Diagnostics.Process PrinterProcess();
        protected string Document { get; set; }

        public static void Print(string document)
        {
            PdfPrinting printer = null;

            switch(Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    printer = LinuxPdfPrinting.GetPrinter(document);
                    break;

                case PlatformID.Win32NT:
                case PlatformID.WinCE:
                    printer = WindowsPdfPrinting.GetPrinter(document);
                    break;

                default:
                    throw new Exception(Catalog.GetString("The printing for this operating system is not supported."));
            }

            printer.Print();
        }
    }



    public class WindowsPdfPrinting : PdfPrinting
    {
        protected WindowsPdfPrinting(string document) : base (document)
        {            
        }

        protected override System.Diagnostics.Process PrinterProcess()
        {
            Assembly assembly = Assembly.GetCallingAssembly();
            string file = assembly.Location;
            file = Directory.GetParent(file).FullName;
            file = Path.Combine(file, "SumatraPDF.exe");

            System.Diagnostics.Process sumatra_pdf = new System.Diagnostics.Process();
            sumatra_pdf.StartInfo.FileName = file;
            sumatra_pdf.StartInfo.Arguments = this.Document;
            sumatra_pdf.StartInfo.Arguments += " -print-dialog -exit-when-done";
            sumatra_pdf.StartInfo.CreateNoWindow = true;
            sumatra_pdf.StartInfo.UseShellExecute = false;

            return sumatra_pdf;

        }

        public static PdfPrinting GetPrinter(string document)
        {
            return new WindowsPdfPrinting(document);
        }
    }



    public class LinuxPdfPrinting : PdfPrinting
    {
        protected LinuxPdfPrinting(string document) : base(document)
        {
        }

        protected override System.Diagnostics.Process PrinterProcess()
        {
            string arguments = null;

            System.Diagnostics.Process gtklp = new System.Diagnostics.Process();
            gtklp.StartInfo.FileName = "gtklp";
            gtklp.StartInfo.Arguments = "-l";
            gtklp.StartInfo.CreateNoWindow = true;
            gtklp.StartInfo.UseShellExecute = false;
            gtklp.StartInfo.RedirectStandardOutput = true;
            gtklp.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;

            gtklp.Start();
            arguments = gtklp.StandardOutput.ReadLine();
            gtklp.WaitForExit();

            if (string.IsNullOrEmpty(arguments))
                return null;

            Console.WriteLine("lpr " + arguments);

            System.Diagnostics.Process lpr = new System.Diagnostics.Process();
            lpr.StartInfo.FileName = "lpr";
            string file_args = this.Document;          
            lpr.StartInfo.Arguments = arguments + " " + file_args;
            lpr.StartInfo.CreateNoWindow = true;
            lpr.StartInfo.UseShellExecute = false;
            return lpr;
        }

        public static PdfPrinting GetPrinter(string document)
        {
            return new LinuxPdfPrinting(document);
        }
    }
}
