// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using Hiperscan.PdfLib;
using System.Reflection;
using System.Collections.Generic; 
using System.IO;
using System.Drawing;

using Hiperscan.Unix;


namespace Hiperscan.Pdf.MergedSubstances
{
    
    public static class MergedCreator
    {
        public static void Create(MergedList list, string pdf_file)
        {
            MergedCreator.Document = new Layout(PdfPageSizes.A4_PORTRAIT, pdf_file);
            MergedCreator.Document.PDF.SetTitle(Catalog.GetString("Overview of merged substances"));
            MergedCreator.Document.PDF.SetSubject(Catalog.GetString("This document contains a list of merged substances."));

            MergedCreator.Document.LowerMargin = 50;

            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("SummarizedSubstances.Resources.Apo_Ident_Logo.png");
            MergedCreator.HiperscanLogo = new Bitmap(s);
                     
            MergedCreator.Table = list;    
            MergedCreator.Document.SimulateDrawing = true;
            {
                MergedCreator.CurrentPageNumber = 1;
                MergedCreator.CreateContents();
                MergedCreator.TotalPageNumber = MergedCreator.CurrentPageNumber;
            }
            
            MergedCreator.Document.SimulateDrawing = false;
            {
                MergedCreator.CurrentPageNumber = 1;
                MergedCreator.CreateContents();
            }

            MergedCreator.Document.Save();
        }


        #region table pages
        private static List<string> PinYinGroups = new List<string>(new string[]
                                                                 {   
                                                                    "herbasinica",
                                                                    "phytocomm",
                                                                    "chinamedica",
                                                                });


        private static int TableHeaderColor                     {get;set;}
        private static int TableRowColor                        {get;set;}
        private static int TableLineColor                         {get;set;}
        private static double VersionWidth                        {get; set;}
    
        private static double NamesEntryColumnWidth                 {get;set;}    
        private static double CellSpace                                {get;set;}
        private static double LineWidth                                {get;set;}
        
        
        private static void CreateContents()
        {
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 10, PdfColors.Black);
            MergedCreator.VersionWidth = MergedCreator.Document.MeasureString(MergedCreator.Table.Version) + 30;
            
            MergedCreator.TableHeaderColor     = 0x00B33333;
            MergedCreator.TableRowColor         = 0x00FFCCCC;
            MergedCreator.TableLineColor         = 0X00E68080;

            MergedCreator.LineWidth = 1;
            MergedCreator.CellSpace = MergedCreator.LineWidth * 3;

            MergedCreator.NamesEntryColumnWidth = (MergedCreator.Document.InnerPageWidth +
                                                        MergedCreator.LineWidth) / 2;
    
            MergedCreator.CreateOnePageForEnumerations();
        }


        private static void CreateOnePageForEnumerations()
        {
            double tmp1, tmp2;
            double top;
            PdfRect rect;
            string text;
            int row = 1;

            
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 10, PdfColors.Black);
            MergedCreator.Document.YPosition = 0;
            MergedCreator.Document.XPosition = 0;
            MergedCreator.Document.AddHeader("HiperScan GmbH");

            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 12, PdfColors.Black);
            MergedCreator.Document.YPosition = 50;
            MergedCreator.Document.XPosition = 0;
            MergedCreator.Document.AddParagraph(MergedCreator.Table.Text1);
            
            top = MergedCreator.Document.ParagraphBoundingBox.Height + MergedCreator.Document.YPosition;
            
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 8, PdfColors.Black);
            MergedCreator.Document.XPosition = 0;
            MergedCreator.Document.YPosition = MergedCreator.Document.InnerPageHeight + 20;
            MergedCreator.Document.AddFooter(MergedCreator.Table.Version);

            MergedCreator.Document.AddHeaderBitmap(MergedCreator.HiperscanLogo, new PdfPoint(MergedCreator.Document.InnerPageWidth - 105, -15), PdfBitmapScales.X, 105);

            
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA_BOLD, 10, PdfColors.White);
            top += 30;
        
            //German column
            rect = new PdfRect(0, top, MergedCreator.NamesEntryColumnWidth, 15);
            MergedCreator.Document.AddRectangle(rect, MergedCreator.LineWidth, 
                                                     MergedCreator.TableLineColor, MergedCreator.TableHeaderColor);

            text = Catalog.GetString("Substance name");
            foreach (string producer in MergedCreator.PinYinGroups)
            {
                string lower_group_name = MergedCreator.Table.ClassName.ToLower();
                if (lower_group_name.Contains(producer) == true)
                {
                    text = Catalog.GetString("Pin Yin");
                    break;
                }
            }

            MergedCreator.Document.YPosition = top + 1;
            MergedCreator.Document.XPosition = 0;
            MergedCreator.Document.AddParagraph(text, rect.Width, PdfTextAlignments.Center);    
            
            
            //Latin Column
            rect = new PdfRect(MergedCreator.NamesEntryColumnWidth - MergedCreator.LineWidth, 
                               top, MergedCreator.NamesEntryColumnWidth, 15);
            MergedCreator.Document.AddRectangle(rect, MergedCreator.LineWidth, 
                                                     MergedCreator.TableLineColor, MergedCreator.TableHeaderColor);

            MergedCreator.Document.XPosition = rect.X;
            MergedCreator.Document.YPosition = top + 1;
            MergedCreator.Document.AddParagraph(Catalog.GetString("Latin name"), rect.Width, PdfTextAlignments.Center);
            

            //page entry
            if (MergedCreator.Document.SimulateDrawing == false)
            {
                MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 8, PdfColors.Black);
                MergedCreator.Document.XPosition = 0;
                MergedCreator.Document.YPosition = MergedCreator.Document.InnerPageHeight + 20;
                string page_number = String.Format(Catalog.GetString("Page {0} of {1}"), MergedCreator.CurrentPageNumber, MergedCreator.TotalPageNumber);
                MergedCreator.Document.AddFooter(page_number, PdfTextAlignments.Right);
            }
            
            
            
            top += 14;
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 10, PdfColors.Black);
            
            
            for(int i = 0; i < MergedCreator.Table.Entries.Count; i++)
            {
                string default_entry   = String.Join("\n", MergedCreator.Table.Entries[i].DefaultNames.ToArray());
                string latin_entry = String.Join("\n",MergedCreator.Table.Entries[i].LatinNames.ToArray());

                int color         = row % 2 == 1 ? MergedCreator.TableRowColor : PdfColors.White;
                
                //cell widths
                double substance_paragraph_width = MergedCreator.NamesEntryColumnWidth - 2 * MergedCreator.CellSpace;

                tmp1 = MergedCreator.Document.MeasureParagraph(default_entry, substance_paragraph_width).Height;
                tmp2 = MergedCreator.Document.MeasureParagraph(latin_entry, substance_paragraph_width).Height;    


                double paragraph_height = tmp1 > tmp2 ? tmp1 : tmp2;
                paragraph_height += 5;
                
                //create first column
                rect = new PdfRect(0, top, MergedCreator.NamesEntryColumnWidth, paragraph_height);
                MergedCreator.Document.AddRectangle(rect, MergedCreator.LineWidth, 
                                                        MergedCreator.TableLineColor, color);

                
                MergedCreator.Document.XPosition = MergedCreator.CellSpace;
                MergedCreator.Document.YPosition = top + 0.5;
                MergedCreator.Document.AddParagraph(default_entry, substance_paragraph_width);
                
                
                //create second column
                rect = new PdfRect(MergedCreator.NamesEntryColumnWidth - MergedCreator.LineWidth, 
                                   top, 
                                   MergedCreator.NamesEntryColumnWidth, paragraph_height);
                MergedCreator.Document.AddRectangle(rect, MergedCreator.LineWidth, 
                                                         MergedCreator.TableLineColor, color);
                
                MergedCreator.Document.XPosition = MergedCreator.CellSpace + 
                                                        MergedCreator.NamesEntryColumnWidth - MergedCreator.LineWidth;
                MergedCreator.Document.AddParagraph(latin_entry, substance_paragraph_width);
    
                top += paragraph_height - MergedCreator.LineWidth;
                row++;
            }

            top += 30;
            MergedCreator.Document.SetFont(PdfFontTypes.HELVETICA, 12, PdfColors.Black);
            MergedCreator.Document.YPosition = top;
            MergedCreator.Document.XPosition = 0;
            MergedCreator.Document.AddParagraph(MergedCreator.Table.Text2);

        } 
        #endregion table pages

        private static Layout Document                 {get; set;}
        private static Bitmap HiperscanLogo         {get; set;}
        private static MergedList Table                {get; set;}
        private static int CurrentPageNumber        {get; set;}
        private static int TotalPageNumber            {get; set;}

        public static string PdfTmpFile
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".pdf");
            }
        }
    }
}

