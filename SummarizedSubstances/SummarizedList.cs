// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using System.Collections.Generic; 


namespace Hiperscan.Pdf.MergedSubstances
{
    
    public class SummarizedEntry  : IComparable
    {
        public List<string> DefaultNames     {get; set;}
        public List<string> LatinNames        {get; set;}

        public SummarizedEntry()
        {
            this.DefaultNames     = new List<string>();
            this.LatinNames        = new List<string>();
        }

        #region IComparable implementation
        int IComparable.CompareTo (object obj)
        {
            if (obj == null)
                return -1;
            
            SummarizedEntry other = (SummarizedEntry) obj;
            string this_default_names     = String.Join(String.Empty, this.DefaultNames.ToArray());
            string other_default_names     = String.Join(String.Empty, other.DefaultNames.ToArray());
            
            return this_default_names.CompareTo(other_default_names);
        }
        #endregion
    
    }

    public class MergedList
    {
        public MergedList ()
        {
            this.Entries         = new List<SummarizedEntry>();
            this.Text1             = String.Empty;
            this.Text2            = String.Empty;
            this.Version         = String.Empty;
            this.ClassName        = String.Empty;
        }


        public void Sort()
        {
            for (int i = 0; i < this.Entries.Count; i++) 
            {
                this.Entries[i].DefaultNames.Sort();
                this.Entries[i].LatinNames.Sort();
            }

            this.Entries.Sort();
        }

        public List<SummarizedEntry> Entries     {get; set;}
        public string Text1                        {get; set;}
        public string Text2                        {get; set;}
        public string Version                    {get; set;}
        public string ClassName                    {get; set;}
    }
}

