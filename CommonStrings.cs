// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;

using Hiperscan.Unix;


namespace Hiperscan.PdfLib
{
    
    public static class CommonStrings
    {
        public static readonly string SUBSTANCE_OVERVIEW_TOC_AVAILABLE_INSERT = Catalog.GetString("Table of Contents - Available Substances with Sample Insert");
        public static readonly string SUBSTANCE_OVERVIEW_TOC_AVAILABLE        = Catalog.GetString("Table of Contents - Available Substances");
        public static readonly string SUBSTANCE_OVERVIEW_TOC_NOT_MEASURABLE   = Catalog.GetString("Table of Contents - Not Measurable Substances");
        public static readonly string SUBSTANCE_OVERVIEW_VERSION_FROM         = Catalog.GetString("Version from");
    }
}
