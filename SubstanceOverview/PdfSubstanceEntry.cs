﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace Hiperscan.Pdf.SubstanceOverview
{
    [DataContract][Serializable ()]
    public class PdfSubstanceEntry : IComparable<PdfSubstanceEntry>
    {
        public PdfSubstanceEntry()
        {
            this.SubstanceId        = string.Empty;
            this.ClassificationName = string.Empty;
            this.DefaultNames        = new List<string>();
            this.LatinNames            = new List<string>();
            this.NumberOfClassificationMembers    = 0;
            this.IsMeasurable        = false;
        }

        public bool IsSingleClassiification
        {
            get
            {
                return this.NumberOfClassificationMembers < 2;
            }
        }

        public PdfSubstanceEntry Clone()
        {
            PdfSubstanceEntry tmp = new PdfSubstanceEntry();
            tmp.ClassificationName = this.ClassificationName;

            tmp.DefaultNames = new List<string>();
            foreach (string default_name in this.DefaultNames)
                tmp.DefaultNames.Add(default_name);

            tmp.LatinNames = new List<string>();
            foreach (string latin_name in this.LatinNames)
                tmp.LatinNames.Add(latin_name);

            tmp.IsMeasurable = this.IsMeasurable;
            tmp.NumberOfClassificationMembers = this.NumberOfClassificationMembers;
            tmp.SubstanceId = this.SubstanceId;

            return tmp;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            if (obj == null)
                return false;

            PdfSubstanceEntry other = obj as PdfSubstanceEntry;
            if (other == null)
                return false;

            return this.SubstanceId == other.SubstanceId;
        }

        public override int GetHashCode()
        {
            return this.SubstanceId.GetHashCode();
        }

        public int CompareTo(PdfSubstanceEntry other)
        {
            int min_count = this.DefaultNames.Count;
            if (other.DefaultNames.Count < min_count)
                min_count = other.DefaultNames.Count;

            for (int i = 0; i < min_count; i++)
            {
                if (this.DefaultNames[i] != other.DefaultNames[i])
                    return this.DefaultNames[i].CompareTo(other.DefaultNames[i]);
            }

            if (this.DefaultNames.Count != other.DefaultNames.Count)
                return this.DefaultNames.Count - other.DefaultNames.Count;

            min_count = this.LatinNames.Count;
            if (other.LatinNames.Count < min_count)
                min_count = other.LatinNames.Count;

            for (int i = 0; i < min_count; i++)
            {
                if (this.LatinNames[i] != other.LatinNames[i])
                    return this.LatinNames[i].CompareTo(other.LatinNames[i]);
            }

            return this.LatinNames.Count - other.LatinNames.Count;
        }

        [DataMember (Name = "SubstanceId")]     
        public string SubstanceId                     {get; set;}
        [DataMember (Name = "ClassificationName")]     
        public string ClassificationName             {get; set;}
        [IgnoreDataMember]
        public int NumberOfClassificationMembers     {get; set;}
        [DataMember (Name = "DefaultNames")]     
        public List<string> DefaultNames             {get; set;}
        [DataMember (Name = "LatinNames")]         
        public List<string> LatinNames                 {get; set;}
        [IgnoreDataMember]    
        public bool IsMeasurable                     {get; set;}
    }
}
