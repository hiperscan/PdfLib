﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.




using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace Hiperscan.Pdf.SubstanceOverview
{
    [DataContract][Serializable ()]
    public class PdfModelClassEntry : IComparable<PdfModelClassEntry>
    {
        
        public PdfModelClassEntry()
        {
            this.ModelClassName    = string.Empty;
            this.Substances        = new List<PdfSubstanceEntry>();
            this.PageNumber        = 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            if (obj == null)
                return false;

            PdfModelClassEntry other = obj as PdfModelClassEntry;
            if (other == null)
                return false;

            return this.ModelClassName == other.ModelClassName;
        }

        public PdfModelClassEntry Clone(bool clone_substances)
        {
            PdfModelClassEntry tmp = new PdfModelClassEntry();

            tmp.ModelClassName = this.ModelClassName;
            tmp.PageNumber = this.PageNumber;

            if (clone_substances == true)
            {
                foreach(PdfSubstanceEntry substance_entry in this.Substances)
                    tmp.Substances.Add(substance_entry.Clone());
            }
            
            return tmp;
        }

        public override int GetHashCode()
        {
            return this.ModelClassName.GetHashCode();
        }

        public int CompareTo(PdfModelClassEntry other)
        {
            return this.ModelClassName.CompareTo(other.ModelClassName);
        }

        [DataMember (Name = "Substances")]    
        public List<PdfSubstanceEntry> Substances     {get; set;}

        [DataMember (Name = "ModelClassName")]    
        public string ModelClassName             {get; set;}

        [IgnoreDataMember]
        public int PageNumber                    {get; set;}
    }
}
